## Projet Budget Conception

J'ai décidé d'utiliser 3 classes, une qui sera "Category", "Budget" et une autre "Stat".
"Budget" contiendra un id, un montant, une date et catégorie. 
"Stat" sera lié avec la classe "Budget" pour récupérer les données du montant pour en faire un pourcentage. Il sera aussi lié avec la classe "Category" pour récuperer les données du catégorie.

Dans mon useCase, comme on peut le voir. 
- Nous pouvons gérer notre budget mensuel en mettant une limite à la sortie, par exemple, je mettrais une limite de 100€ pour la categorie restaurant par mois.
- Pouvoir choisir une categorie est faculatif lorque qu'on insère un montant. 
- Pouvoir accéder et regarder les stats via les catégories et les montants déjà présents. Le pourcentage changera par rapport aux montants.

